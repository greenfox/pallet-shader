shader_type canvas_item;

uniform vec3 hsvWeights = vec3(1,1,1);



uniform vec4 color1:hint_color;
uniform vec4 color2:hint_color;
uniform vec4 color3:hint_color;
uniform vec4 color4:hint_color;
uniform vec4 color5:hint_color;

vec3 rgb2hsv(vec3 input)
{
	vec3 c = input;
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
	vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 input)
{
	vec3 n_out6p0;
	vec3 c = input;
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}



float hueDist(float h0, float h1)
{
	return min(abs(h1-h0), 1.0-abs(h1-h0));
}
float hsvDist(vec3 a, vec3 b){
	vec3 dis = vec3(hueDist(a.r,b.r),vec2(a.gb-b.gb))* hsvWeights;
	return length(dis);
}

void fragment() {
	vec4 colors[] = {color1,color2,color3,color4,color5};
	
	vec3 inputColor = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0).rgb;
	float dist = 999999999.9;
	int outColorIndex = -1;
	
	vec3 inputHSV = hsv2rgb(inputColor);
	for(int i = 0 ; i < 5; i ++)
	{
		vec3 c = rgb2hsv(colors[i].rgb);
		float newDist = hsvDist(c,inputHSV);
		if (dist > newDist)
		{
			dist = newDist;
			outColorIndex = i;
		} 
	}
	
//	float hd = min(abs(h1-h0), 360-abs(h1-h0));
	COLOR.rgb = colors[outColorIndex].rgb;
	

}
