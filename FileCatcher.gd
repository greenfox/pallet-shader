class_name FileCatcher
extends CanvasLayer

export var maxImageSize :=Vector2(1000,500)

#signal fileCaught(file)
signal addPallet(pallet)
signal addSample(image)

var img = Image.new()

var imageScale := Vector2.ZERO

export var texturePreview:NodePath
export var colorCount:NodePath


func _ready():
	get_tree().connect("files_dropped",self,"catchFile")

func catchFile(data,value):
	print("File cought!!",data)
	#make more genaric if you don't want an image
	img.load(data[0])
	var texture = ImageTexture.new()
	texture.create_from_image(img)
	caughtImage(texture)
	get_node(colorCount).value = 0

func caughtImage(texture:ImageTexture):
	$WindowDialog.popup()
	var p = texture.get_size()
	var imageScale :=Vector2.ONE
	if p.x > maxImageSize.x:
		imageScale.x = maxImageSize.x/p.x
	if p.y > maxImageSize.y:
		imageScale.y = maxImageSize.y/p.x
	imageScale = Vector2.ONE * min(imageScale.x,imageScale.y)
	#pick smaller scale, make equal
	get_node(texturePreview).texture = texture
#	$CenterContainer/PanelContainer/MarginContainer/VBoxContainer/TextureRect.rect_scale = imageScale

#func _process(delta):
#	$CenterContainer/PanelContainer/MarginContainer/VBoxContainer/TextureRect.rect_scale = imageScale



func _on_ImportAsPallet_pressed():
	if get_node(colorCount).value:
		$WindowDialog.visible = false
		var count = get_node(colorCount).value
		var t = ImageTexture.new()
		t.create_from_image(img)
		var p = Pallet.new()
		p.setup("ImprotedPallet",t,count)
		p.image = t
		emit_signal("addPallet",p)

func _on_ImportAsSample_pressed():
	$WindowDialog.visible = false
	emit_signal("addSample",get_node(texturePreview).texture)
