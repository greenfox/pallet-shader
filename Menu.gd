extends CanvasLayer

signal set_pallet(pallet)
signal changeInterp(type)
signal changeHSV(hsv)
signal changeDitherType(ditherType)

var pallets = []

export var visible:bool=true

func _ready():
	reset()

func _process(delta):
	if Input.is_action_just_pressed("hide"):
		visible = not visible
		for i in get_children():
			i.visible = visible


func _on_OptionButton_item_selected(index):
	var p = Pallets.all[index]
	$MarginContainer.visible = p.name != "No pallet!"
	emit_signal("set_pallet",p)
	pass # Replace with function body.


func reset():
	var p = $PalletPicker/MarginContainer/VBoxContainer/PalletPicker
	p.clear()
	for i in Pallets.all:
		pallets.push_back(i)
		p.add_icon_item(i.image,i.name)
	emit_signal("set_pallet",Pallets.all[0])
	emit_signal("changeInterp",0)


func _on_InterType_item_selected(index):
	emit_signal("changeInterp",index)
	var ditherOption := $MarginContainer/VBoxContainer/ditherController
	if $MarginContainer/VBoxContainer/InterType.get_item_text(index) == "Dithering":
		ditherOption.visible = true
	else:
		ditherOption.visible = false
	pass # Replace with function body.


func _resetHSV(_value):
	var h = $PalletPicker/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Hue.value / 360.0
	var s = $PalletPicker/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Stat.value / 50.0
	var v = $PalletPicker/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/Value.value / 50.0
	emit_signal("changeHSV",Vector3(h,pow(2,s-1),pow(2,v-1)))


func _on_ditherController_setDitherType(type):
	emit_signal("changeDitherType",type)
