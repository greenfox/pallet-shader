shader_type spatial;
render_mode unshaded;
render_mode cull_disabled;

uniform sampler2D pallet;
uniform int palletCount = 5;
uniform vec3 hsvAdjust = vec3(.5,1,1);
uniform int renderMode : hint_range(-5, 2);

uniform int ditherType = 0;// : hint_range(-1, 3)
uniform sampler2D ditherTexture;
uniform float ditherRes = 2.0;

uniform bool preferSmallest = false;

uniform vec2 resolution = vec2(1280,720);
uniform bool useOriginalAlpha = false;

uniform float outline = 0.01;
uniform vec4 outlineColor : hint_color;

float abcLerp(vec3 A, vec3 B, vec3 C){
	vec3 AB = B-A;//	var AB = B-A;
	vec3 AC = C-A;//	var AC = C-A;
	vec3 ABn = normalize(AB);//	var ABn = AB.normalized()
	float ABl = length(AB);//	var ABl = max(AB.length(),0.00001)
	return clamp(dot(AC,ABn)/ABl,0f,1f);//	return clamp(AC.dot(ABn)/ABl,0,1)
}

vec3 rgb2hsv(vec3 input)
{
	vec3 c = input;
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
	vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 input)
{
	vec3 n_out6p0;
	vec3 c = input;
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void fragment()
{
	vec3 outcolor;
	vec2 uv = 0.5 - abs(UV-.5);
	if (uv.x < outline || uv.y < outline){
		ALBEDO = outlineColor.rgb;
	}
else {
	
	vec4 screenColor = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.0);
	vec3 inputColor = screenColor.rgb;
	inputColor = rgb2hsv(inputColor);
//	inputColor.r = 0.0;
	inputColor.r += hsvAdjust.r;
	inputColor.gb *= hsvAdjust.gb;
	inputColor = hsv2rgb(inputColor);

	float distA = 999999999.9;
	vec3 pointA;
	float distB = 999999999.9;
	vec3 pointB;
	
	float halfSampleOffset = 0.5/float(palletCount);
	
	for(int i = 0 ; i < palletCount; i ++)
	{
		vec3 color = texture(pallet,vec2(float(i)/float(palletCount)+halfSampleOffset,0.5)).rgb;
		float newDist = length(inputColor.rgb - color.rgb);//hsvDist(c,inputHSV);
		if (distA > newDist)
		{
			distB = distA;
			pointB = pointA;
			distA = newDist;
			pointA = color;
		}
		else if (distB > newDist)
		{
			distB = newDist;
			pointB = color;
		}
	}
	if ( renderMode == -1){
		outcolor.rgb = pointA;
	}
	else if ( renderMode == -2){
		outcolor.rgb = pointB;
	}
	else if ( renderMode == -3){
		outcolor.rgb = vec3(abcLerp(pointA,pointB,inputColor));
	}
	else if ( renderMode == -4){
		uv = mod(SCREEN_UV * resolution,ditherRes)/ditherRes; //mod(SCREEN_UV * ditherRes,resolution);
		outcolor.rgb = texture(ditherTexture,uv).rrr;
	}
	else if ( renderMode == -5){
		outcolor.rgb = inputColor;
	}
	else if ( renderMode == 1){
		outcolor.rgb = mix(pointA,pointB,abcLerp(pointA,pointB,inputColor));
	}
	else if (renderMode==2){
		if (preferSmallest){
			float Al = length(pointA);
			float Bl = length(pointB);
			if( Al > Bl ){
				vec3 temp = pointA;
				pointA = pointB;
				pointB = temp;
			}
		}
		
		float lerpValue = abcLerp(pointA,pointB,inputColor);
		float ditherValue = 0.5;
		
		switch (ditherType){
			case -1: //texture
				uv = mod(SCREEN_UV * resolution,ditherRes)/ditherRes; //mod(SCREEN_UV * ditherRes,resolution);
				ditherValue = texture(ditherTexture,uv).r;
				break;
			case 0: //nearest
				ditherValue = 0.5;
				break;
			case 1: //sine
				vec2 ditherUV = SCREEN_UV * resolution/ (3.1415 * 20.0);
				ditherValue = (sin(ditherUV.x*ditherRes)+sin(ditherUV.y*ditherRes)+ 2.0) / 4.0;
				break;
		}
		outcolor.rgb = mix(pointA,pointB,floor(lerpValue + ditherValue));
	}
	else{
		outcolor.rgb = pointA;
	}
	ALBEDO = outcolor;
}	
}