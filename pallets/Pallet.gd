extends Node
class_name Pallet

export var image:Texture
export var colorCount:int


func setup(pallet_name:String,t:Texture,count:int):
	name = pallet_name
	image = t
	colorCount = count
