class_name PalletButton

extends MarginContainer

signal _change_pallet

var thisPallet:Pallet= null

func setup(pallet:Pallet):
	$PalletSelector/Label.text = pallet.name
	$PalletSelector/TextureRect.texture = pallet.image
	thisPallet = pallet

func _on_Button_pressed():
	emit_signal("_change_pallet",thisPallet)
