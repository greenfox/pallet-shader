extends Node

class_name DitherType

enum TYPE {
	snapping = -3,
	lerping = -2,
	texture = -1,
#	nearest = 0,
	sineWave = 1
}

export(TYPE) var type = TYPE.snapping
export(Texture) var texture
export var defaultScale :float= 50

var resolution:float





